import img from "./assets/images/48.jpg"
import style from "./App.module.css"

function App() {
  return (
    <div className={style.devcampContainer}>
       <div>
        <img src={img} alt="ảnh" className={style.devcampAvatar}></img>
      </div>
      <div className={style.devcampQuote}>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className={style.devcampUser}>
      <b>Tammy Stevens</b> * Front End developer 
      </div>
    </div>
  );
}

export default App;
